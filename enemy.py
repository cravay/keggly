import pygame, game, directionentity, json

# TODO: add enemies to the leveleditor

STANDING_STILL = 4

def init():
	'''loads the enemy image'''
	global enemytypes
	enemytypes = []
	
	file = open('images/enemies.json')
	
	for data in json.loads(file.read()):
		enemytypes.append(EnemyType(data['name'], data['path']))

class EnemyType():
	def __init__(self, name, path):
		self.name = name
		self.image = pygame.image.load('images/' + path).convert_alpha()
	
class Enemy(directionentity.DirectionEntity):
	'''Everything related to the enemies'''

	def __init__(self, path, type):
		super().__init__(enemytypes[type].image)

		self.path = path
		self.path_position = 0
		
		self.x = path[0][0] * 20
		self.y = path[0][1] * 20 - self.rect.height + 20

		self.update_walking_direction()
		
	def update_walking_direction(self):
		if len(self.path) == 1:
			self.walking_direction = STANDING_STILL
		elif self.path[self.path_position][1] == self.path[(self.path_position + 1) % len(self.path)][1]:
			if self.path[self.path_position][0] < self.path[(self.path_position + 1) % len(self.path)][0]:
				self.walking_direction = directionentity.LOOKING_RIGHT
			else:
				self.walking_direction = directionentity.LOOKING_LEFT
		else:
			if self.path[self.path_position][1] < self.path[(self.path_position + 1) % len(self.path)][1]:
				self.walking_direction = directionentity.LOOKING_DOWN
			else:
				self.walking_direction = directionentity.LOOKING_UP

		
	def update(self):
		if self.walking_direction == STANDING_STILL:
			pass	
		elif self.walking_direction == directionentity.LOOKING_UP:
				self.move_up()
				if self.y + self.rect.height - 20 <= self.path[(self.path_position + 1) % len(self.path)][1] * 20:
					self.path_position = (self.path_position + 1) % len(self.path)
					self.update_walking_direction()
		elif self.walking_direction == directionentity.LOOKING_DOWN:
				self.move_down()
				if self.y + self.rect.height - 20 >= self.path[(self.path_position + 1) % len(self.path)][1] * 20:
					self.path_position = (self.path_position + 1) % len(self.path)
					self.update_walking_direction()
		elif self.walking_direction == directionentity.LOOKING_LEFT:
				self.move_left()
				if self.x - 20 + self.rect.width <= self.path[(self.path_position + 1) % len(self.path)][0] * 20:
					self.path_position = (self.path_position + 1) % len(self.path)
					self.update_walking_direction()
		else:
				self.move_right()
				if self.x >= self.path[(self.path_position + 1) % len(self.path)][0] * 20:
					self.path_position = (self.path_position + 1) % len(self.path)
					self.update_walking_direction()
		
		super().update()
		
	def draw(self):
		game.display.blit(self.image, self.rect)