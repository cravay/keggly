# keggly
In keggly you play keggly the frog and fight against evil rats. The game is under construction.

## dependencies
[Python 3.2](https://www.python.org/downloads/)  
[Pygame 1.9.2](http://www.pygame.org/download.shtml)

## leveleditor
A leveleditor for the game can be found [here](http://cravay.gitlab.io/keggly/leveleditor.html).

![Image of the Leveleditor](http://cravay.gitlab.io/keggly/leveleditor.png)

## screenshots
![Screenshot 1](http://cravay.gitlab.io/keggly/screenshot_1.png)
![Screenshot 2](http://cravay.gitlab.io/keggly/screenshot_2.png)
![Screenshot 3](http://cravay.gitlab.io/keggly/screenshot_3.png)
![Screenshot 4](http://cravay.gitlab.io/keggly/screenshot_4.png)
![Screenshot 5](http://cravay.gitlab.io/keggly/screenshot_5.png)
![Screenshot 6](http://cravay.gitlab.io/keggly/screenshot_6.png)
