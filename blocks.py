import pygame, json

blocks = []

def init():
	image = pygame.image.load('images/blocks.bmp').convert()

	file = open('images/blocks.json')
	
	for block in json.loads(file.read()):
		blocks.append(Block(image, block['xpos'], block['ypos'], block['hitbox']))

class Block():
	'''Class that contains a block'''
	def __init__(self, surface, xpos, ypos, hitbox):
		self.image = pygame.Surface((20, 20))
		self.image.blit(surface, pygame.Rect(0, 0, 20, 20), pygame.Rect(xpos * 20, ypos * 20, 20, 20))
		self.hitbox = hitbox
