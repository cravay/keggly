import pygame, blocks, enemy, game

class Screen(pygame.sprite.Sprite):
	'''The screen class contains a part of a level (e.g. blocks, enemies)'''
	def __init__(self, data):
		super().__init__()

		self.warps = data[0]
		self.enemies = []
		for enemy_data in data[1]:
			self.enemies.append(enemy.Enemy(enemy_data[1], enemy_data[0]))
		self.block_grid = data[2]
		
		self.height = len(self.block_grid)
		self.width = len(self.block_grid[0])
		
		self.image = pygame.Surface((20 * self.width, 20 * self.height))
		self.rect = pygame.Rect(0, 0, 20 * self.width, 20 * self.height)

		self.mask = pygame.mask.Mask((self.rect.width, self.rect.height))
		for y in range(self.height):
			for x in range(self.width):
				if blocks.blocks[self.block_grid[y][x]].hitbox:
					for i in range(20):
						for ii in range(20):
							self.mask.set_at((20 * x + i, 20 * y + ii))

		for y in range(self.height):
			for x in range(self.width):
				self.image.blit(blocks.blocks[self.block_grid[y][x]].image,
				pygame.Rect(20 * x, 20 * y, 20, 20))
	
	def update(self):
		'''updates all the enemies'''
		for enemy in self.enemies:
			enemy.update()
	
	def draw(self):
		'''draws the level on the screen'''		
		game.display.blit(self.image, self.rect)

		for enemy in self.enemies:
			enemy.draw()
