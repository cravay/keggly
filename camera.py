import game, math

def update():
	if game.player.y + game.player.half_height > 240:
		if game.current_screen.rect.height - game.player.half_height - game.player.y > 240:
			game.player.rect.y = 240 - math.floor(game.player.rect.height / 2)
			game.current_screen.rect.y = - game.player.y + 240 - game.player.half_height
		else:
			game.player.rect.y = game.player.y - game.current_screen.rect.height + 480
			game.current_screen.rect.y = 480 - game.current_screen.rect.height
	else:
		game.player.rect.y = game.player.y
		game.current_screen.rect.y = 0
	
	if game.player.x + game.player.half_width > 320:
		if game.current_screen.rect.width - game.player.half_width - game.player.x > 320:
			game.player.rect.x = 320 - math.floor(game.player.rect.width / 2)
			game.current_screen.rect.x = - game.player.x + 320 - game.player.half_width
		else:
			game.player.rect.x = game.player.x - game.current_screen.rect.width + 640
			game.current_screen.rect.x = 640 - game.current_screen.rect.width
	else:
		game.player.rect.x = game.player.x
		game.current_screen.rect.x = 0
		
	for enemy in game.current_screen.enemies:
		enemy.rect.x = enemy.x + game.current_screen.rect.x
		enemy.rect.y = enemy.y + game.current_screen.rect.y