import pygame, game

'''Direction Constants'''
LOOKING_DOWN = 0;
LOOKING_UP = 1;
LOOKING_RIGHT = 2;
LOOKING_LEFT = 3;

'''Walking state Constants'''
STANDING_STILL = 0
WALKING_1 = 1
WALKING_2 = 2

class DirectionEntity(pygame.sprite.Sprite):
	'''Base class for Entities that can face in four directions'''
	def __init__(self, surface):
		super().__init__()
		
		self.x = 0
		self.y = 0
		
		self.spritesheet = surface
			
		sprite_rect = self.spritesheet.get_rect()
			
		self.rect = pygame.Rect(0, 0, sprite_rect.width / 4, sprite_rect.height / 3)
		
		self.image = pygame.Surface((self.rect.width, self.rect.height), pygame.SRCALPHA)
		self.direction = LOOKING_DOWN
		self.walking_state = STANDING_STILL
		self.has_moved = False
		self.walking_timer = 0
		
		self.mask = pygame.mask.Mask((self.rect.width, self.rect.height))
		for i in range(self.rect.width):
			for ii in range(4):
				self.mask.set_at((i, self.rect.height - 1 - ii), 1)
		
	def update(self):
		if self.has_moved:
			if self.walking_state == STANDING_STILL:
				self.walking_state = WALKING_1
				self.walking_timer = 0
			else:
				self.walking_timer += 1
				if self.walking_timer == 5:
					self.walking_state = self.walking_state % 2 + 1
					self.walking_timer = 0
		else:
			self.walking_state = STANDING_STILL
		
		self.image.fill(pygame.Color(0, 0, 0, 0))
		self.image.blit(self.spritesheet, pygame.Rect(0, 0, self.rect.width, self.rect.height), (self.direction * self.rect.width, self.walking_state * self.rect.height, self.rect.width, self.rect.height))
		
		self.has_moved = False
	
	def move(self):
		self.has_moved = True
	
	def move_up(self):
		self.direction = LOOKING_UP
		for i in range(2):
			self.rect.move_ip(0, -1)
			self.y -= 1
			if pygame.sprite.collide_mask(self, game.current_screen) or self.y + self.rect.height < 4:
				self.rect.move_ip(0, 1)
				self.y += 1
				return		
		self.move()

	def move_down(self):
		self.direction = LOOKING_DOWN
		for i in range(2):
			self.rect.move_ip(0, 1)
			self.y += 1
			if pygame.sprite.collide_mask(self, game.current_screen) or self.y + self.rect.height > game.current_screen.rect.height:
				self.rect.move_ip(0, -1)
				self.y -= 1
				return		
		self.move()

	def move_left(self):
		self.direction = LOOKING_LEFT
		for i in range(2):
			self.rect.move_ip(-1, 0)
			self.x -= 1
			if pygame.sprite.collide_mask(self, game.current_screen) or self.x < 0:
				self.rect.move_ip(1, 0)
				self.x += 1
				return
		self.move()

	def move_right(self):
		self.direction = LOOKING_RIGHT
		for i in range(2):
			self.rect.move_ip(1, 0)
			self.x += 1
			if pygame.sprite.collide_mask(self, game.current_screen) or self.x + self.rect.width > game.current_screen.rect.width:
				self.rect.move_ip(-1, 0)
				self.x -= 1
				return		
		self.move()