import pygame, game, sys

# Game initialization
game.init()
game.load_screen(0)

clock = pygame.time.Clock()

# Gameloop
while True:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			sys.exit()

	game.update()
	game.draw()

	pygame.display.flip()

	clock.tick(30)