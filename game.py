import pygame, json, blocks, screen, player, enemy, camera

def init():
	pygame.init()

	global display
	display = pygame.display.set_mode((640, 480))

	pygame.display.set_caption("Keggly")
	icon = pygame.image.load("images/icon.bmp").convert_alpha()
	pygame.display.set_icon(icon)

	global player 
	player = player.Player()
	blocks.init()
	enemy.init()

	global screens
	screens = []

	file = open('levels/default.json')
			
	data = json.loads(file.read())

	global checkpoint
	checkpoint = (data[0][0] * 20,data[0][1] * 20 - player.rect.height + 20, 0)
	player.x = checkpoint[0]
	player.y = checkpoint[1]

	for screen_data in data[1]:
		screens.append(screen.Screen(screen_data))
	
	file.close()

		
def update():
	player.update()
	current_screen.update()
	for enemy in current_screen.enemies:
		if player.rect.colliderect(enemy):
			load_screen(checkpoint[2])
			player.x = checkpoint[0]
			player.y = checkpoint[1]
	camera.update()
	
def draw():
	current_screen.draw()
	player.draw()

	
def load_screen(screen_id):
	global current_screen
	current_screen = screens[screen_id]