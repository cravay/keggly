import pygame, directionentity, game, sys, math

class Player(directionentity.DirectionEntity):
	'''Everything related to the player'''

	def __init__(self):
		super().__init__(pygame.image.load('images/keggly.bmp').convert_alpha())
		
		self.has_warped = False

		self.half_height = math.floor(self.rect.height / 2)
		self.half_width =  math.floor(self.rect.width / 2)

	def draw(self):
		game.display.blit(self.image, self.rect)
		
	def update(self):
		pressed_keys = pygame.key.get_pressed()

		if pressed_keys[pygame.K_DOWN]:
			self.move_down()
		elif pressed_keys[pygame.K_UP]:
			self.move_up()
		elif pressed_keys[pygame.K_LEFT]:
			self.move_left()
		elif pressed_keys[pygame.K_RIGHT]:
			self.move_right()
		elif pressed_keys[pygame.K_ESCAPE]:
			pygame.quit()
			sys.exit()

		super().update()
		
		#warping
		collision = False
		for warp in game.current_screen.warps:
			if self.x + 8 >= warp[0][0] * 20 and \
			   self.x + 8 < warp[0][0] * 20 + 20 and \
			   self.y + 12 + 8 >= warp[0][1] * 20 and \
			   self.y + 12 + 8 < warp[0][1] * 20 + 20:

				if not self.has_warped:
					game.load_screen(warp[1])
					self.x = self.x - warp[0][0] * 20 + warp[2][0] * 20
					self.y = self.y - warp[0][1] * 20 + warp[2][1] * 20

					game.checkpoint = (self.x, self.y, warp[1])
				
				collision = True
				break
			
		if collision:
			self.has_warped = True
		else:
			self.has_warped = False
